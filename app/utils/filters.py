from app.app import app


@app.template_filter("key_idx")
def get_item_idx(dict, key):
    keys = list(dict.keys())
    return keys.index(key)
