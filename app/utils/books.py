books = {
    "alfonsine_astronomy": {
        "author": ["CHABAS José", "HUSSON Matthieu", "KREMER Richard"],
        "title": "Alfonsine Astronomy: The Written Record",
        "volume": "",
        "publisher": "Brepols",
        "place": "",
        "year": "2022",
        "pages": "426p.",
        "url": "",
    },
}

articles = {
    "": {
        "author": "",
        "title": "",
        "journal": "",
        "publisher": "",
        "year": "",
        "pages": "",
        "url": "",
    },
}
