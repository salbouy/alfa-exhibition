# from app.utils.manuscripts import bnf7286C
from app.utils.paths import manuscripts_trails

persons = {
    "albumazar": {
        "name": "Abu Ma'shar",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 237v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=ywWveU59hwK7G4Aap3er"
            },
            manuscripts_trails["bnf7286c"]["header"]: {
                "f. 55r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=qfI07VfoYKZX59dOJlEa"
            },
            manuscripts_trails["basel7"]["header"]: {"f. 82r": ""},
        },
        "id_bliblissima": "Q624",
    },
    "albategnius": {
        "name": "Al Batani",
        "url": "https://en.wikipedia.org/wiki/Al-Battani",
        "biography": "",
        "attention_points": {
            manuscripts_trails["erfurt377"]["header"]: {
                "f. 38r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=dO9f7xQCKJOup4jbh6ht"
            }
        },
        "id_biblissima": "Q12153",
    },
    "alpetragius": {
        "name": "Al Bitruji",
        "url": "https://en.wikipedia.org/wiki/Nur_ad-Din_al-Bitruji",
        "biography": "",
        "attention_points": {
            manuscripts_trails["erfurt377"]["header"]: {
                "f. 17r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=CADVK7oZIpyycUYpQL4b"
            }
        },
        "id_biblissima": "Q82446",
    },
    "alfraganus": {
        "name": "Al Farghani",
        "url": "https://en.wikipedia.org/wiki/Al-Farghani",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "binding": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=72NGzM3YbzYBxWJ69jRh",
                "f. 1r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=gtvKTjmFd33YkkIwaHJf",
            },
        },
        "id_biblissima": "Q513",
    },
    "arzachel": {
        "name": "Al Zarqali",
        "url": "https://en.wikipedia.org/wiki/Ab%C5%AB_Is%E1%B8%A5%C4%81q_Ibr%C4%81h%C4%ABm_al-Zarq%C4%81l%C4%AB",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 237v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=ywWveU59hwK7G4Aap3er",
            },
        },
        "id_biblissima": "Q7271",
    },
    "arnaud-de-bruxelles": {
        "name": "Arnaud de Bruxelles",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7270"]["header"]: {
                "binding": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=U1UQr46csuCKVyELYuaP",
                "f. 1r": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=O3KsCTLr2P37ioo7s00P",
            }
        },
        "id_biblissima": "",
    },
    "arnaudi-pratoris": {
        "name": "Arnaudi Pratoris",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=gtvKTjmFd33YkkIwaHJf"
            },
        },
        "id_biblissima": "",
    },
    "bernhard-walther": {
        "name": "Bernhard Walther",
        "url": "https://en.wikipedia.org/wiki/Bernhard_Walther",
        "biography": "",
        "attention_points": {manuscripts_trails["vienna5203"]["header"]: {"f. 1r": ""}},
        "id_biblissima": "Q2494",
    },
    "campanus-of-novara": {
        "name": "Campanus of Novara",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7432"]["header"]: {
                "f. 210v": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=MyJQUbFzKJRpxKuXOdRE"
            },
            manuscripts_trails["pallat1375"]["header"]: {"f. 263r ?": ""},
        },
        "id_biblissima": "Q2731",
    },
    "heingarter": {
        "name": "Conrad Heingarter",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7197"]["header"]: {
                "top edge": "https://www.exhibit.so/exhibits/RzwuABCwZJKudbEJwYS0?screen=A0ekwPDDkaEu31lh03Si",
                "f. 31v": "https://www.exhibit.so/exhibits/RzwuABCwZJKudbEJwYS0?screen=2H6bC1GQHFubPdGyHHRz",
                "f. 35r": "https://www.exhibit.so/exhibits/RzwuABCwZJKudbEJwYS0?screen=Ddt5iyJjlThMV11IKZXx",
                "f. 50r": "https://www.exhibit.so/exhibits/RzwuABCwZJKudbEJwYS0?screen=yPdoZP2Rg1BmSMU7IOQJ",
                "f. 118r": "https://www.exhibit.so/exhibits/RzwuABCwZJKudbEJwYS0?screen=NVgWYRvlje4cHwmo1HU9",
            },
            manuscripts_trails["bnf7295a"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=EhAvE97zBedBf9f7e8ay",
                "f. 49r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=kJaowopzJJG3BGV3YvAh",
                "f. 79r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=iB6h3c7bAzBgxVEemkJP",
                "f. 118v": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=i9PdfEWMsJtdyaKpNLRn",
                "f. 176v": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=1RPshCobpOYSdm6r5Nzl",
            },
            manuscripts_trails["bnf7432"]["header"]: {
                "f. 3v": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=061n34lLSfxG0gxXxKdR",
                "f. 126r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=ZdA9jeJ7U5ofyUoKkFj4",
                "f. 219r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=eQymWcYef0EVfGwWhLVO",
                "f. 270r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=uKARE4DTaAxT6QXXuYwt",
            },
        },
        "id_biblissima": "Q3316",
    },
    "firmin-de-beauval": {
        "name": "Firmin of Beauval",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 175r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=HC5zKeKhAkJSOwy2An8a"
            }
        },
        "id_biblissima": "Q4688",
    },
    "geoffrey-of-meaux": {
        "name": "Geoffrey of Meaux",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 160v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=3XA975P3KpCD8kXlKoSj"
            }
        },
        "id_biblissima": "",
    },
    "gerard-of-cremona": {
        "name": "Gerard of Cremona",
        "url": "https://en.wikipedia.org/wiki/Gerard_of_Cremona",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=gtvKTjmFd33YkkIwaHJf"
            },
            manuscripts_trails["bnf7432"]["header"]: {
                "f. 210v": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=MyJQUbFzKJRpxKuXOdRE"
            },
        },
        "id_biblissima": "Q5639",
    },
    "bianchini": {
        "name": "Giovanni Bianchini",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7270"]["header"]: {
                "binding": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=U1UQr46csuCKVyELYuaP",
                "inside cover": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=k2AO8AntTwLPvgGC6Xxr",
                "f. 152v": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=3DU2PA54jlMLEd700aFE",
                "f. 228r": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=SeGCiXUrwfujuY9HSj5T",
                "f. 233v": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=SeGCiXUrwfujuY9HSj5T",
            },
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 44r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=bN2da4e9TVtsA8HN1jpo"
            },
            manuscripts_trails["pallat1375"]["header"]: {"f. 90r": "", "f. 262v": ""},
        },
        "id_bliblissima": "Q5893",
    },
    "guilhelmus-copp": {
        "name": "Guilhelmus Copp",
        "url": "https://en.wikipedia.org/wiki/Guillaume_Cop",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7432"]["header"]: {
                "f. 270r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=uKARE4DTaAxT6QXXuYwt"
            }
        },
        "id_biblissima": "Q6293",
    },
    "jacques-despars": {
        "name": "Jacques Despars",
        "url": "https://fr.wikipedia.org/wiki/Jacques_Despars",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 279v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=swLhIrPMn7v0bJ5GIw8F"
            }
        },
        "id_biblissima": "Q8069",
    },
    "jacques-maci": {
        "name": "Jacques Maci",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=UGuK2V5jq4GIyvVFzIKu"
            }
        },
        "id_biblissima": "",
    },
    "johannes-campanus": {
        "name": "Johannes Campanus",
        "url": "https://en.wikipedia.org/wiki/Johann_Campanus",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 49r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=kJaowopzJJG3BGV3YvAh"
            }
        },
        "id_biblissima": "",
    },
    "sacrobosco": {
        "name": "Johannes de Sacrobosco",
        "url": "https://en.wikipedia.org/wiki/Johannes_de_Sacrobosco",
        "biography": "",
        "attention_points": {
            manuscripts_trails["escorial10"]["header"]: {"f. 28r": ""}
        },
        "id_biblissima": "Q9134",
    },
    "virdung": {
        "name": "Johannes of Virdung",
        "url": "https://en.wikipedia.org/wiki/Johannes_Virdung",
        "biography": "",
        "attention_points": {
            manuscripts_trails["pallat1375"]["header"]: {
                "f. 17vr": "",
                "f. 23v": "",
                "f. 43v": "",
                "f. 90r": "",
                "f. 269r": "",
                "f. 269v": "",
            }
        },
        "id_biblissima": "Q304718",
    },
    "schoner": {
        "name": "Johannes Schöner",
        "url": "https://en.wikipedia.org/wiki/Johannes_Sch%C3%B6ner",
        "biography": "",
        "attention_points": {
            manuscripts_trails["vienna5203"]["header"]: {
                "f. 1r": "",
                "f. 14r": "",
                "f. 17v": "",
                "f. 29v": "",
                "f. 54r": "",
                "f. 119r": "",
            }
        },
        "id_biblissima": "",
    },
    "avis": {
        "name": "John Avis",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 282v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=rBqc4f4DRk2ZftfarTX6"
            }
        },
        "id_biblissima": "",
    },
    "genoa": {
        "name": "John of Genoa",
        "url": "https://en.wikipedia.org/wiki/John_of_Genoa",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 160v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=3XA975P3KpCD8kXlKoSj"
            },
            manuscripts_trails["bnf7286c"]["header"]: {
                "f. 56v": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=PN7gihIayFdLFLLFwEQW"
            },
        },
        "id_biblissima": "Q8434",
    },
    "gmunden": {
        "name": "John of Gmunden",
        "url": "https://en.wikipedia.org/wiki/Johannes_von_Gmunden",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7478"]["header"]: {
                "f": "https://www.exhibit.so/exhibits/askiIZU32YfzrJYamwUl?screen=nooGpV6DnloZUFzENU6Q"
            }
        },
        "id_biblissima": "Q243721",
    },
    "ligneres": {
        "name": "John of Lignères",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["basel7"]["header"]: {
                "f. 38v": "",
            },
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 160v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=3XA975P3KpCD8kXlKoSj"
            },
            manuscripts_trails["bnf7286c"]["header"]: {
                "f. 10v": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=WZYJtXFYJQtinOk7qiHj",
                "f. 23v": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=rDHoxRdkksbz7hiHyiuf",
                "f. 25v": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=j3s42feJhkcIrx1nUlaY",
                "f. 28r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=KAa2ZfuOh7VThjUcJ4MG",
                "f. 29r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=L1S8tO8xg9pmMPuG4ruc",
                "f. 54r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=64Q6XikzLkbBYavm1Iho",
                "f. 55r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=qfI07VfoYKZX59dOJlEa",
                "f. 56v": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=PN7gihIayFdLFLLFwEQW",
                "f. 57r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=ipDnggZLDsaSk1dFBna3",
            },
            manuscripts_trails["bnf7295a"]["header"]: {
                "f. 49r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=kJaowopzJJG3BGV3YvAh",
                "f. 176v": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=1RPshCobpOYSdm6r5Nzl",
            },
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 5v": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=I8QpBxbjwdbmgDYRYP2o",
                "f. 34r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=XrNMjg21shNXzVaZKsuB",
                "f. 44r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=xtNg2u4ZieO4fKsFATmb",
                "f. 72v": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=hCLwAlRmNfVp2abxHfAP",
            },
            manuscripts_trails["erfurt377"]["header"]: {
                "f. 22v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=yvdxTnioVDnvxZqZeNyC",
                "f. 23v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=3yDWRYAJvfSdpQOQLbkL",
                "f. 35r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=nQGggbMXURqOYZycOEou",
                "f. 35v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=VNYc76Kv9wtPYt1yYz86",
            },
            manuscripts_trails["escorial10"]["header"]: {"f. 18r": ""},
            manuscripts_trails["pallat1375"]["header"]: {"f. 8v": ""},
        },
        "id_biblissima": "Q8459",
    },
    "murs": {
        "name": "John of Murs",
        "url": "https://en.wikipedia.org/wiki/Johannes_de_Muris",
        "biography": "",
        "attention_points": {
            manuscripts_trails["basel7"]["header"]: {"AP4": "", "f. 83v": ""},
            manuscripts_trails["bnf7270"]["header"]: {
                "f. 77v": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=dDMlwwy8O3RepcttBhYb"
            },
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 156v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=uTA7XKZBdtuairNwhqSM"
            },
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 44r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=bN2da4e9TVtsA8HN1jpo",
                "f. 175r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=HC5zKeKhAkJSOwy2An8a",
            },
            manuscripts_trails["erfurt377"]["header"]: {
                "f. 35v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=VNYc76Kv9wtPYt1yYz86",
                "f. 38r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=dO9f7xQCKJOup4jbh6ht",
            },
            manuscripts_trails["escorial10"]["header"]: {
                "f. 26r": "",
                "f. 63v": "",
                "f. 92v": "",
                "f. 98r": "",
                "f. 123r": "",
                "f. 159r": "",
                "f. 204v": "",
                "f. 217v": "",
                "f. 221v": "",
                "f. 225r": "",
            },
        },
        "id_biblissima": "Q8475",
    },
    "saxony": {
        "name": "John of Saxony",
        "url": "https://en.wikipedia.org/wiki/John_of_Saxony_(astronomer)",
        "biography": "",
        "attention_points": {
            manuscripts_trails["basel7"]["header"]: {"AP4": ""},
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 160v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=3XA975P3KpCD8kXlKoSj"
            },
            manuscripts_trails["bnf7295a"]["header"]: {
                "f. 149r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=kJaowopzJJG3BGV3YvAh"
            },
            manuscripts_trails["erfurt377"]["header"]: {
                "f. 22v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=yvdxTnioVDnvxZqZeNyC",
                "f. 23v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=3yDWRYAJvfSdpQOQLbkL",
                "f. 35r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=nQGggbMXURqOYZycOEou",
                "f. 35v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=VNYc76Kv9wtPYt1yYz86",
                "f. 38r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=dO9f7xQCKJOup4jbh6ht",
            },
            manuscripts_trails["escorial10"]["header"]: {"f. 204v": ""},
        },
        "id_biblissima": "Q9078",
    },
    "sevilla": {
        "name": "John of Sevilla",
        "url": "https://en.wikipedia.org/wiki/John_of_Seville",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=gtvKTjmFd33YkkIwaHJf"
            }
        },
        "id_biblissima": "Q8499",
    },
    "sicily": {
        "name": "John of Sicily",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 17r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=eFCk0g1I5FefTq9ODCGP",
                "f. 146v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=Tki8Guse0IW5Z4F4KO0A",
            }
        },
        "id_biblissima": "",
    },
    "troyes": {
        "name": "Johannes de Troyes",
        "url": "https://fr.wikipedia.org/wiki/Jean_Lesguis%C3%A9",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 235r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=elfEp1LtjZOsvJC0rkuw",
            }
        },
        "id_biblissima": "Q229351",
    },
    "vimond": {
        "name": "John Vimond",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7270"]["header"]: {
                "f. 77v": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=dDMlwwy8O3RepcttBhYb"
            },
            manuscripts_trails["bnf7286c"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=UU5Bw7HO6pp10y3V8M7N",
                "f. 1v": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=OI9jRQLyfiYpXu4XWBJb",
                "f. 2r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=2vDol0nJt3dy0uk9fnBo",
                "f. 57r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=ipDnggZLDsaSk1dFBna3",
            },
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 44r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=xtNg2u4ZieO4fKsFATmb"
            },
            manuscripts_trails["erfurt377"]["header"]: {
                "f. 22r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=pu5NEQ9h8vcmqhg25jIm",
                "f. 35v": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=VNYc76Kv9wtPYt1yYz86",
            },
        },
        "id_biblissima": "",
    },
    "louis-lepuiset": {
        "name": "Loys Lepuiset ",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 282v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=rBqc4f4DRk2ZftfarTX6"
            }
        },
        "id_biblissima": "Q25404",
    },
    "thomas-magister": {
        "name": "Magister Thomas",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["pallat1375"]["header"]: {"f. 269v": ""}
        },
        "id_biblissima": "Q16156",
    },
    "messahala": {
        "name": "Masha'allah",
        "url": "https://en.wikipedia.org/wiki/Mashallah_ibn_Athari",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7295a"]["header"]: {
                "f. 49r": "https://www.exhibit.so/exhibits/UOxGzvYitXfSoHYxDNaB?screen=kJaowopzJJG3BGV3YvAh"
            },
            manuscripts_trails["bnf7432"]["header"]: {
                "f. 151v": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=yKC5XUOSdPZ28RszrSSu"
            },
        },
        "id_biblissima": "Q11454",
    },
    "meton-of-athens": {
        "name": "Meton of Athens",
        "url": "https://en.wikipedia.org/wiki/Meton_of_Athens",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7478"]["header"]: {
                "NP": "https://www.exhibit.so/exhibits/askiIZU32YfzrJYamwUl?screen=FfdRa3L7FCu6ZxfYw1JM"
            }
        },
        "id_biblissima": "",
    },
    "michael-roscher": {
        "name": "Michael Roscher",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7432"]["header"]: {
                "f. 219r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=eQymWcYef0EVfGwWhLVO"
            }
        },
        "id_biblissima": "",
    },
    "nicholaus-of-heybech": {
        "name": "Nicholaus of Heybech",
        "url": "",
        "biography": "",
        "attention_points": {manuscripts_trails["basel7"]["header"]: {"f. ": ""}},
        "id_biblissima": "",
    },
    "nicole-oresme": {
        "name": "Nicole Oresme",
        "url": "https://en.wikipedia.org/wiki/Nicole_Oresme",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7197"]["header"]: {
                "f. 79v": "https://www.exhibit.so/exhibits/RzwuABCwZJKudbEJwYS0?screen=MgicV9bbA1v3UcVyHosL"
            },
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=8RKK85jE9qLgEFEUVQXP",
                "f. 259r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=f2yOCNPt80hDW68lV1vZ",
            },
        },
        "id_biblissima": "Q12721",
    },
    "patrus-de-sancto-audomaro": {
        "name": "Patrus de Sancto Audomaro",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["vienna5203"]["header"]: {"f. 119r": ""}
        },
        "id_biblissima": "",
    },
    "peter-of-dacia": {
        "name": "Peter of Dacia",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 146v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=Tki8Guse0IW5Z4F4KO0A"
            },
            manuscripts_trails["bnf7286c"]["header"]: {
                "f. 57r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=ipDnggZLDsaSk1dFBna3"
            },
        },
        "id_biblissima": "Q13651",
    },
    "peter-of-saint-omer": {
        "name": "Peter of Saint Omer",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7286c"]["header"]: {
                "f. 54r": "https://www.exhibit.so/exhibits/tPKyQQcpctVpM4tzsTnu?screen=64Q6XikzLkbBYavm1Iho"
            }
        },
        "id_biblissima": "",
    },
    "peurbach": {
        "name": "Peurbach",
        "url": "https://en.wikipedia.org/wiki/Georg_von_Peuerbach",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7270"]["header"]: {
                "f. 233v": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=V5Al7mpA3EZ2AluRB3gA"
            },
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 175r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=HC5zKeKhAkJSOwy2An8a"
            },
            manuscripts_trails["pallat1375"]["header"]: {"f. 54": "", "f. 90": ""},
            manuscripts_trails["vienna5203"]["header"]: {
                "f. 1r": "",
                "f. 3": "",
                "f. 14r": "",
            },
        },
        "id_biblissima": "Q5629",
    },
    "pseudo-ptolemy": {
        "name": "Pseudo Ptolemy",
        "url": "",
        "biography": "",
        "attention_points": {
            manuscripts_trails["erfurt377"]["header"]: {
                "f. 55r": "https://www.exhibit.so/exhibits/BwvTP09SMZy4mmJr4W6h?screen=uxmDNYS3RNyS1klv4oFi"
            }
        },
        "id_biblissima": "Q3211",
    },
    "ptolemy": {
        "name": "Ptolemy",
        "url": "https://en.wikipedia.org/wiki/Ptolemy",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7270"]["header"]: {
                "f. 152v": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=3DU2PA54jlMLEd700aFE"
            },
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 1r": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=3DU2PA54jlMLEd700aFE"
            },
            manuscripts_trails["bnf7432"]["header"]: {
                "binding": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=ZREevXRUWxzc1Hs5xohp",
                "f. 4r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=BltclN16mKYHVherUaQZ",
                "f. 34r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=9RG87MilHqYIYS7LxvUn",
                "f. 66r": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=lfgEWpfJlWmDkwx4wFFJ",
                "f. 103v": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=asWU0NjIZop8fxwS5jaO",
                "f. 134v": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=yJCKAHBYxhuuSal0Dmcc",
                "210v": "https://www.exhibit.so/exhibits/PWEZjtMe2Gz1XGkNNhei?screen=MyJQUbFzKJRpxKuXOdRE",
            },
            manuscripts_trails["escorial10"]["header"]: {"f. 69r": ""},
        },
        "id_biblissima": "Q3210",
    },
    "regiomontanus": {
        "name": "Regiomontanus",
        "url": "https://en.wikipedia.org/wiki/Regiomontanus",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7270"]["header"]: {
                "f. 233v": "https://www.exhibit.so/exhibits/fNbgnIVNxmM7QxVF1v8l?screen=V5Al7mpA3EZ2AluRB3gA"
            },
            manuscripts_trails["pallat1375"]["header"]: {"f. 269r": ""},
            manuscripts_trails["vienna5203"]["header"]: {
                "f. 1r": "",
                "f. 3": "",
                "f. 14r": "",
                "f. 29v": "",
                "f. 70r": "",
            },
        },
        "id_biblissima": "Q9379",
    },
    "robert-bardis": {
        "name": "Robert Bardis",
        "url": "https://en.wikipedia.org/wiki/Robert_de_Bardis",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 34r": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=XrNMjg21shNXzVaZKsuB"
            }
        },
        "id_biblissima": "",
    },
    "simon-de-phares": {
        "name": "Simon de Phares",
        "url": "https://fr.wikipedia.org/wiki/Simon_de_Phares",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f.  282v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=rBqc4f4DRk2ZftfarTX6"
            }
        },
        "id_biblissima": "Q15340",
    },
    "thebit": {
        "name": "Thabit Ibn Quara",
        "url": "https://en.wikipedia.org/wiki/Th%C4%81bit_ibn_Qurra",
        "biography": "",
        "attention_points": {
            manuscripts_trails["escorial10"]["header"]: {"f. 82r": ""}
        },
        "id_biblissima": "Q271",
    },
    "william-batecombe": {
        "name": "William Batecombe",
        "url": "https://en.wikipedia.org/wiki/William_Batecumbe",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnfcol60"]["header"]: {
                "f. 72v": "https://www.exhibit.so/exhibits/QDcZVOynCMcRwHpoh8GS?screen=hCLwAlRmNfVp2abxHfAP"
            }
        },
        "id_biblissima": "",
    },
    "william-of-saint-cloud": {
        "name": "William of Saint Cloud",
        "url": "https://en.wikipedia.org/wiki/William_of_Saint-Cloud",
        "biography": "",
        "attention_points": {
            manuscripts_trails["bnf7281"]["header"]: {
                "f. 17r": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=eFCk0g1I5FefTq9ODCGP",
                "f. 146v": "https://www.exhibit.so/exhibits/pByGoZQTs7Y0d5RA1EXk?screen=Tki8Guse0IW5Z4F4KO0A",
            }
        },
        "id_biblissima": "",
    },
}
