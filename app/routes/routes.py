import random

from flask import render_template, request
import json


from app.utils.const import (
    APP_NAME,
)

from app.utils.paths import (
    main_pages,
    all_themes,
    heritage_trails,
    historical_trails,
    scientific_trails,
    manuscripts_trails,
    must_see,
)

from app.utils.persons import (
    persons,
)

from app.utils.books import (
    books,
    articles,
)

from app.utils.utils import empty_dir, check_extension, remove_item

from app.app import app


def log(variable):
    if isinstance(variable, (dict, list)):
        app.logger.info(json.dumps(variable, indent=4))
    else:
        app.logger.info(variable)


@app.context_processor
def global_jinja_variables():
    variable_name_in_python = "coucou"
    return {"app_title": APP_NAME, "variable_name_in_template": variable_name_in_python}


@app.route("/", methods=["GET"])
def home():
    return render_template(
        "pages/index.html",
        title="Index",
        theme=main_pages["home"],
        carousel_items=must_see,
        path_desc=all_themes,
    )


def theme_page(title, theme_name, theme_trails, themes=all_themes):
    trails = list(theme_trails.values())
    random.shuffle(trails)
    return render_template(
        "pages/theme.html",
        title=title,
        id=theme_name,
        theme=themes[theme_name],
        cards=trails,
        carousel_items=remove_item(themes, theme_name),
    )


def trail_page(title, trail_name, trails, theme):
    ap_id = request.args.get("screen")
    return render_template(
        "pages/trail.html",
        title=title,
        id=trail_name,
        ap_id=ap_id,
        trail=trails[trail_name],
        carousel_items=remove_item(trails, trail_name),
        theme=theme,
    )


@app.route("/heritage", methods=["GET"])
def heritage():
    return theme_page("Heritage", "heritage", heritage_trails)


@app.route("/heritage/exceptional", methods=["GET"])
def exceptional():
    return trail_page(
        "Exceptional manuscript", "exceptional", heritage_trails, all_themes["heritage"]
    )


@app.route("/heritage/seeing", methods=["GET"])
def seeing():
    return trail_page(
        "Seeing the manuscript", "seeing", heritage_trails, all_themes["heritage"]
    )


@app.route("/heritage/touching", methods=["GET"])
def touching():
    return trail_page(
        "Touching the manuscript", "touching", heritage_trails, all_themes["heritage"]
    )


@app.route("/heritage/typical", methods=["GET"])
def typical():
    return trail_page(
        "Typical manuscripts", "typical", heritage_trails, all_themes["heritage"]
    )


# @app.route("/heritage/presentation", methods=["GET"])
# def presentation():
#     return trail_page("Presentation manuscript", "presentation", heritage_trails)
#
#
# @app.route("/heritage/student", methods=["GET"])
# def student():
#     return trail_page("Student manuscript", "student", heritage_trails)
#
#
# @app.route("/heritage/toolbox", methods=["GET"])
# def toolbox():
#     return trail_page("Toolbox manuscript", "toolbox", heritage_trails)
#
#
# @app.route("/heritage/university", methods=["GET"])
# def university():
#     return trail_page("University manuscript", "university", heritage_trails)


@app.route("/historical", methods=["GET"])
def historical():
    return theme_page("Historical", "historical", historical_trails)


# @app.route("/historical/alandalus", methods=["GET"])
# def alandalus():
#     return trail_page("Royal origins in al-Andalus", "alandalus", historical_trails)


@app.route("/historical/europe", methods=["GET"])
def europe():
    return trail_page(
        "Fostering astronomical activities around Europe",
        "europe",
        historical_trails,
        all_themes["historical"],
    )


@app.route("/historical/paris", methods=["GET"])
def paris():
    return trail_page(
        "Paris university", "paris", historical_trails, all_themes["historical"]
    )


@app.route("/historical/print", methods=["GET"])
def print():
    return trail_page(
        "Transition to the printing world",
        "print",
        historical_trails,
        all_themes["historical"],
    )


@app.route("/scientific", methods=["GET"])
def scientific():
    return theme_page("Scientific", "scientific", scientific_trails)


@app.route("/scientific/disciplines", methods=["GET"])
def disciplines():
    return trail_page(
        "Other disciplines", "disciplines", scientific_trails, all_themes["scientific"]
    )


@app.route("/scientific/eclipse", methods=["GET"])
def eclipse():
    return trail_page("Eclipse", "eclipse", scientific_trails, all_themes["scientific"])


@app.route("/scientific/planetary", methods=["GET"])
def planetary():
    return trail_page(
        "Planetary astronomy", "planetary", scientific_trails, all_themes["scientific"]
    )


@app.route("/scientific/spherical", methods=["GET"])
def spherical():
    return trail_page(
        "Spherical astronomy", "spherical", scientific_trails, all_themes["scientific"]
    )


# @app.route("/scientific/tables", methods=["GET"])
# def tables():
#     return trail_page("Astronomical tables", "tables", scientific_trails)
#
#
# @app.route("/scientific/texts", methods=["GET"])
# def texts():
#     return trail_page("Texts", "texts", scientific_trails)
#
#
# @app.route("/scientific/diagrams", methods=["GET"])
# def diagrams():
#     return trail_page("Diagrams and instruments", "diagrams", scientific_trails)


@app.route("/manuscripts", methods=["GET"])
def manuscripts():
    return theme_page("Manuscripts", "manuscripts", manuscripts_trails)


@app.route("/manuscripts/basel7", methods=["GET"])
def basel7():
    return trail_page(
        "Basel F II 7", "basel7", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7197", methods=["GET"])
def bnf7197():
    return trail_page(
        "BnF Lat. 7197", "bnf7197", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7270", methods=["GET"])
def bnf7270():
    return trail_page(
        "BnF Lat. 7270", "bnf7270", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7276a", methods=["GET"])
def bnf7276a():
    return trail_page(
        "BnF Lat. 7276A", "bnf7276a", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7281", methods=["GET"])
def bnf7281():
    return trail_page(
        "BnF Lat. 7281", "bnf7281", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7286c", methods=["GET"])
def bnf7286c():
    return trail_page(
        "BnF Lat. 7286C", "bnf7286c", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7295a", methods=["GET"])
def bnf7295a():
    return trail_page(
        "BnF Lat. 7295A", "bnf7295a", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7432", methods=["GET"])
def bnf7432():
    return trail_page(
        "BnF Lat. 7432", "bnf7432", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnf7478", methods=["GET"])
def bnf7478():
    return trail_page(
        "BnF Lat. 7478", "bnf7478", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/bnfcol60", methods=["GET"])
def bnfcol60():
    return trail_page(
        "BnF Mel.Col. 60", "bnfcol60", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/erfurt377", methods=["GET"])
def erfurt377():
    return trail_page(
        "Erfurt F. 377", "erfurt377", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/escorial10", methods=["GET"])
def escorial10():
    return trail_page(
        "Escorial O. II. 10",
        "escorial10",
        manuscripts_trails,
        all_themes["manuscripts"],
    )


@app.route("/manuscripts/madrid3306", methods=["GET"])
def madrid3306():
    return trail_page(
        "Madrid BNE 3306", "madrid3306", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/pallat1375", methods=["GET"])
def pallat1375():
    return trail_page(
        "Pal. Lat. 1375", "pallat1375", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/manuscripts/vienna5203", methods=["GET"])
def vienna5203():
    return trail_page(
        "Vienna ÖNB 5203", "vienna5203", manuscripts_trails, all_themes["manuscripts"]
    )


@app.route("/about", methods=["GET"])
def about():
    return render_template(
        "pages/about.html",
        title="About",
        theme=main_pages["about"],
    )


@app.route("/bibliography", methods=["GET"])
def bibliography():
    list_books = sorted(books.values(), key=lambda d: d["author"], reverse=False)
    list_articles = sorted(articles.values(), key=lambda d: d["author"])
    return render_template(
        "pages/bibliography.html",
        title="Bibliography",
        books=list_books,
        articles=list_articles,
        theme=main_pages["bibliography"],
    )


@app.route("/glossary", methods=["GET"])
def glossary():
    list_persons = sorted(persons.values(), key=lambda d: d["name"])
    return render_template(
        "pages/glossary.html",
        title="Astronomers",
        persons=list_persons,
        theme=main_pages["glossary"],
    )
