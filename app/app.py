from flask import Flask, render_template

from app.utils.const import TEMPLATE_DIR, STATIC_DIR, APP_NAME, CONFIG

app = Flask(__name__, template_folder=TEMPLATE_DIR, static_folder=STATIC_DIR)


# import must be made after app initialisation, this import allows routes to be loaded even if the variable is not used
from app.routes import routes


def config_app(config_name="dev"):
    """Create the application"""
    app.config.from_object(CONFIG[config_name])
    return app


if __name__ == "__main__":
    # config_app()
    app.run(debug=True)
